%Transiciones
tr('A').
tr('B').
tr('C').
tr('D').
tr('E').

%Lugares
pl(p0).
pl(p1).
pl(p2).
pl(p3).
pl(p4).
pl(p5).

%Arcos
arc(p0, 'A').
arc('A', p1).
arc('A', p2).
arc(p1, 'B').
arc(p2, 'C').
arc('B', p3).
arc('C', p4).
arc('D', p2).
arc(p3, 'E').
arc(p4,'D').
arc(p4,'E').
arc('E', p5).

%Marcado inicial
m0([p0]).

%preset
preset(Nodo,R):- setof(X, arc(X,Nodo), Ac), R = Ac.

%postset 
postset(Nodo,R):- setof(X, arc(Nodo,X), Ac), R = Ac.

%is enabled
isEnabled(Trans,M):- preset(Trans,R), subset(R,M).

%fire
fire(Trans,M,Nm):- isEnabled(Trans,M) ->  
    preset(Trans,R),
    subtract(M,R,Rest),
    postset(Trans,R2),
    ord_union(Rest,R2,Nm)
    ; Nm = M.

%Enablement
enablement(M,Tr):-
    findall(X,isEnabled(X,M),Tr).

%Replay
replay([],_).
replay([H|T],Nm):- 
    isEnabled(H,Nm),
    fire(H,Nm,New),
    replay(T,New).

replay(List):- 
    m0(M),
    replay(List,M).